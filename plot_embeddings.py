#!/usr/bin/env python
# coding: utf-8

# In[187]:


import argparse
import os
import pickle
import tkinter
# https://matplotlib.org/api/pyplot_api.html
import matplotlib.pyplot as plt
# https://scikit-learn.org/stable/modules/generated/sklearn.manifold.TSNE.html
from sklearn.manifold import TSNE
# https://plot.ly/python/line-and-scatter/
import plotly
import plotly.plotly as py
import plotly.graph_objs as go


# In[188]:


def plot_with_labels(low_dims, labels, file_name, args):
  assert low_dims.shape[0] >= len(labels), 'More labels than embeddings'

  title = 'Iterations: ' +  str(args.n_iter) + ' Learn: ' + str(args.learning_rate) + ' Perplexity: ' + str(args.perplexity)
  x_es, y_es, texts = [[], [], [], []], [[], [], [], []], [[], [], [], []]
  colors = ['204, 0, 153', '0, 204, 153', '204, 204, 0', '0, 153, 153']

  fig = plt.figure(figsize=(10, 10))
  for i, label in enumerate(labels):
    x, y = low_dims[i, :]
    if x > 0:
      if y > 0:
        x_es[0].append(x)
        y_es[0].append(y)
        texts[0].append(label)
      else:
        x_es[1].append(x)
        y_es[1].append(y)
        texts[1].append(label)
    else:
      if y > 0:
        x_es[2].append(x)
        y_es[2].append(y)
        texts[2].append(label)
      else:
        x_es[3].append(x)
        y_es[3].append(y)
        texts[3].append(label)

    plt.scatter(x, y)
    plt.annotate(label, xy=(x, y), xytext=(5, 2), textcoords='offset points', ha='right', va='bottom')

  layout = dict(title = title)

  data = list(range(4))
  for i in range(4):
    data[i] = go.Scatter(
      x = x_es[i],
      y = y_es[i],
      text = texts[i],
      mode = 'markers',
      marker = dict(size=10, opacity=0.5, color='rgba(' + colors[i] + ', .9)')
    )

  figure = dict(data=data, layout=layout)
  html_file = file_name + '.html'
  plotly.offline.plot(figure, filename=html_file)

  plt.title(title)
  plt.savefig(file_name)
  plt.close(fig)


# In[189]:


def launch_model(model_name):
  model_dir = './models'
  model_path = os.path.join(model_dir, '%s.model' % model_name)

  return pickle.load(open(model_path, 'rb'))


# In[190]:


def parse_corpus(model_name):
  final_embeddings, dictionary, reverse_dictionary = launch_model(model_name)
  # Récupère seulement les mots de 2 à 4 caractères
  labels, embeddings_to_plot = [], []
  for index, value in enumerate(dictionary):
    if len(value) >= 2 and len(value) <= 4:
      embeddings_to_plot.append(final_embeddings[index])
      labels.append(reverse_dictionary[index])

  print('embeddings_to_plot[:2]', embeddings_to_plot[:2])
  print('len(embeddings_to_plot)', len(embeddings_to_plot))

  return labels, embeddings_to_plot


# In[191]:


def main(args, labels, embeddings):
  print(args)

  try:
    tsne = TSNE(
      init=args.init,
      learning_rate=args.learning_rate,
      method=args.method,
      n_components=args.n_components,
      n_iter=args.n_iter,
      perplexity=args.perplexity
    )

    plot_only = args.plot_only
    low_dims = tsne.fit_transform(embeddings[:plot_only])
    labels = [labels[i] for i in range(plot_only)]
    image_name = 'word2vec' + '_iter-' + str(args.n_iter) + '_learn-' + str(args.learning_rate) + '_perplex-' + str(args.perplexity)

    directory = os.path.join('./plots/%s/' % args.plot_only)
    print(directory)
    if not os.path.exists(directory):
        os.makedirs(directory)
    plot_with_labels(low_dims, labels, os.path.join(directory, '%s.png' % image_name), args)

  except ImportError as ex:
    print('Please install sklearn, matplotlib, and scipy to show embeddings.')
    print(ex)


# In[192]:


class FakeArguments:
  def __init__(self, perplexity, learning_rate, n_iter, plot_only):
    self.perplexity = perplexity
    self.learning_rate = learning_rate
    self.n_iter = n_iter
    self.plot_only = plot_only
    self.n_components = 2
    self.init = 'pca'
    self.method = 'exact'


# In[193]:


if __name__ == '__main__':
  plot_only = 150
  perplexities = [1, 2, 3, 4, 5]
  learning_rates = [1, 5, 10, 25, 50]
  n_iters = [250, 500, 1000]

  labels, embeddings_to_plot = parse_corpus('word2vec')
  # main(FakeArguments(perplexities[0], learning_rates[0], n_iters[0], 50), labels, embeddings_to_plot)

  for perplexity in perplexities:
    for learning_rate in learning_rates:
      for n_iter in n_iters:
        main(FakeArguments(perplexity, learning_rate, n_iter, plot_only), labels, embeddings_to_plot)


# In[ ]:




