import torch
from torch import nn

class SkipGram(nn.Module):
    def __init__(self, vocab_size, embedding_dim, verbose=0):
        super(SkipGram, self).__init__()
        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.verbose = verbose

        # v_embeddings for center words
        self.v_embeddings = nn.Embedding(vocab_size, embedding_dim)

        # u_embeddings for context words
        self.u_embeddings = nn.Embedding(vocab_size, embedding_dim)

        # Init parameters for u and v if train from scratch
        self.v_embeddings.weight.data.uniform_(-1.0, 1.0)
        self.u_embeddings.weight.data.normal_()

    def forward(self, pos_v, pos_u, neg_u):
        """
        :param pos_v: (batch_size) long tensor variables of input (center) word ids
        :param pos_u: (batch_size) long tensor variables of label (context) word ids
        :param neg_u: (batch_size, num_neg) long tensor variables of negative word ids for each pos_v
        :return: The cross entropy loss between u and v
                 minimize L  = -log[exp(pos_u'*pos_v) / sum_i(exp(neg_u'*pos_v))]
                             =  log(sum_i(exp(neg_u'*pos_v))) - pos_u'*pos_v
                             =  log(neg_score) - pos_score
        """
        pos_v_embedding = self.v_embeddings(pos_v)
        pos_u_embedding = self.u_embeddings(pos_u)
        neg_u_embedding = self.u_embeddings(neg_u)

        # Similarity between pos_u and pos_v, i.e., pos_u'*pos_v (dot product between pos_u and pos_v)
        # pos_u_embedding: (batch_size, embedding_dim) -> (batch_size, 1, embedding_dim)
        # pos_v_embedding: (batch_size, embedding_dim) -> (batch_size, embedding_dim, 1)
        # pos_score: (batch_size) each value is pos_u'*pos_v
        pos_score = torch.bmm(pos_u_embedding.view(-1, 1, self.embedding_dim),
                              pos_v_embedding.view(-1, self.embedding_dim, 1)).squeeze()
        if self.verbose:
            print('pos_score:', pos_score.size())
            print(pos_score.data)

        # Similarity between neg_u and pos_v, i.e., dot product between each neg_u and pos_v
        # neg_u_embedding: (batch_size, num_neg, embedding_dim)
        # pos_v_embedding: (batch_size, embedding_dim) -> (batch_size, embedding_dim, 1)
        # neg_score: (batch_size, num_neg) each value is neg_u'*pos_v
        neg_score = torch.bmm(neg_u_embedding, pos_v_embedding.view(-1, self.embedding_dim, 1)).squeeze()

        if self.verbose:
            print('neg_score:', neg_score.size())
            print(neg_score.data)

        # Compute log(sum_i(exp(neg_u'*pos_v)))
        # neg_score: (batch_size, num_neg)
        # log_neg_score: (batch_size)
        log_neg_score = torch.log(torch.sum(torch.exp(neg_score), dim=1))

        # Compute final score then take average, i.e., score = log(neg_score) - pos_score
        # log_neg_score: (batch_size)
        # pos_score: (batch_size)
        # score: scalar for loss
        score = log_neg_score - pos_score
        score = torch.mean(score)

        return score

    def forward_u(self, u):
        return self.u_embeddings(u)

    def forward_v(self, v):
        return self.v_embeddings(v)

    def set_u_embeddings(self, weights):
        self.u_embeddings.weight.data.copy_(torch.from_numpy(weights))

    def set_v_embeddings(self, weights):
        self.v_embeddings.weight.data.copy_(torch.from_numpy(weights))

    def get_u_embeddings(self):
        return self.u_embeddings.weight.data

    def get_v_embeddings(self):
        return self.v_embeddings.weight.data