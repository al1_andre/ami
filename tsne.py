#!/usr/bin/env python
# coding: utf-8

import argparse
import os
import pickle
import tkinter
# https://matplotlib.org/api/pyplot_api.html
import matplotlib.pyplot as plt
# https://scikit-learn.org/stable/modules/generated/sklearn.manifold.TSNE.html
from sklearn.manifold import TSNE

def plot_with_labels(low_dims, labels, file_name):
  assert low_dims.shape[0] >= len(labels), 'More labels than embeddings'

  plt.figure(figsize=(25, 25))
  for i, label in enumerate(labels):
    x, y = low_dims[i, :]
    plt.scatter(x, y)
    plt.annotate(label, xy=(x, y), xytext=(5, 2), textcoords='offset points', ha='right', va='bottom')
  plt.savefig(file_name)

def main(args):
  print(args)
  model_dir = './models'
  model_name = 'word2vec_pt'
  model_path = os.path.join(model_dir, '%s.model' % model_name)
  final_embeddings, dictionary, reverse_dictionary = pickle.load(open(model_path, 'rb'))

  try:
    tsne = TSNE(
      init=args.init,
      learning_rate=args.learning_rate,
      method=args.method,
      n_components=args.n_components,
      n_iter=args.n_iter,
      perplexity=args.perplexity
    )

    plot_only = args.plot_only
    low_dims = tsne.fit_transform(final_embeddings[:plot_only, :])
    labels = [reverse_dictionary[i] for i in range(plot_only)]
    image_name = 'word2vec_init-' + str(args.init) + '_learn-' + str(args.learning_rate) + '_iter-' + str(args.n_iter) + '_perplex-' + str(args.perplexity)
    plot_with_labels(low_dims, labels, os.path.join('./plots', '%s.png' % image_name))

  except ImportError as ex:
    print('Please install sklearn, matplotlib, and scipy to show embeddings.')
    print(ex)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  # The perplexity is related to the number of nearest neighbors that is used in other manifold learning algorithms. Larger datasets usually require a larger perplexity. Consider selecting a value between 5 and 50. Different values can result in significanlty different results.
  parser.add_argument('--perplexity', type=float, default=30)
  # The learning rate for t-SNE is usually in the range [10.0, 1000.0]. If the learning rate is too high, the data may look like a ‘ball’ with any point approximately equidistant from its nearest neighbours. If the learning rate is too low, most points may look compressed in a dense cloud with few outliers. If the cost function gets stuck in a bad local minimum increasing the learning rate may help.
  parser.add_argument('--learning_rate', type=float, default=200)
  # Dimension of the embedded space.
  parser.add_argument('--n_components', type=int, default=2)
  # Initialization of embedding. Possible options are ‘random’, ‘pca’, and a numpy array of shape (n_samples, n_components). PCA initialization cannot be used with precomputed distances and is usually more globally stable than random initialization.
  parser.add_argument('--init', type=str, default='random')
  # Number of iterations run.
  parser.add_argument('--n_iter', type=int, default=5000)
  # By default the gradient calculation algorithm uses Barnes-Hut approximation running in O(NlogN) time. method=’exact’ will run on the slower, but exact, algorithm in O(N^2) time. The exact algorithm should be used when nearest-neighbor errors need to be better than 3%. However, the exact method cannot scale to millions of examples.
  parser.add_argument('--method', type=str, default='exact')
  # plot_only
  parser.add_argument('--plot_only', type=int, default=1000)
  main(parser.parse_args())
