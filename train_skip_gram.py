#!/usr/bin/env python
# coding: utf-8

import os
import pickle
import argparse
import numpy as np

import torch
from torch.autograd import Variable

from sklearn.preprocessing import normalize
from skip_gram import SkipGram
from nlp_tools import NlpTools

import matplotlib.pyplot as plt

torch.manual_seed(1)
np.set_printoptions(precision=2)

def download_data(file):
  print("Étape 1 : Téléchargement des données d'entrée", file)
  raw_text = open(file).read().split()
  print('\tTaille des données :', len(raw_text))

  return raw_text

def hyper_parameters():
  print("Étape 0 : paramétrage de l'hyperparamètre")
  os.environ['CUDA_VISIBLE_DEVICES'] = str(3)

def build_dictionary(words, iterations=1000, verbose=0, learning_rate=1):
  print('Étape 2 : Construction du dictionnaire de mots')
  data, count, dictionary, reverse_dictionary, args = NlpTools.build_skipgram_dataset(words, iterations, verbose, learning_rate)

  print('\n\tMots les plus communs (+UNK) :', count[:5])
  print('\tÉchantillon (10 premiers) :', [reverse_dictionary[i] for i in data[:10]])

  return data, count, dictionary, reverse_dictionary, args

def make_training_batch(data, index, reverse_dictionary):
  print("Étape 3 : Fonction de test pour la génération d'un lot d'entraînement pour le modèle skip-gram")
  batch, labels, _ = NlpTools.generate_batch(data=data, index=index, batch_sz=8, n_skips=2, skip_sz=1)
  for i in range(8):
    print('\t', batch[i], reverse_dictionary[batch[i]], '->', labels[i], reverse_dictionary[labels[i]])

  return batch, labels

def create_model(args, os, verbose):
  print("Étape 4 : Construction du modèle Skip Gram")
  net = SkipGram(args.vocabulary_size, args.embedding_size, verbose)

  # Load pretrained weights
  pretrained_file = './weights/w2v.model'
  if os.path.exists(pretrained_file):
    v_weights, u_weights = pickle.load(open(pretrained_file, 'r'))
    net.set_v_embeddings(v_weights)
    net.set_u_embeddings(u_weights)

  # Optimizer for parameters
  optimizer = torch.optim.SGD(net.parameters(), lr=args.learning_rate)

  # Cuda
  if args.cuda:
    net.cuda()

  # valid examples for checking
  valid_examples = np.random.choice(args.valid_window, args.valid_size, replace=False)
  batch_valid = torch.LongTensor(valid_examples)
  batch_valid = Variable(batch_valid.cuda()) if args.cuda else Variable(batch_valid)

  return net, optimizer, valid_examples, batch_valid

def train_model(elements, verbose=0):
  print("Étape 5 : Entraînement du modèle Skip Gram")
  avg_loss = 0
  inps, tgts, negs, losses = [], [], [], []

  for step in range(elements['args'].num_steps):
    batch_mids, batch_lbls, index = NlpTools.generate_batch(elements['data'], elements['index'], elements['args'].batch_size, elements['args'].num_skips, elements['args'].skip_window)

    # batch_inps: 1d array: (batch_size)
    batch_inps = np.squeeze(batch_mids)

    # batch_tgts: 1d array: (batch_size)
    batch_tgts = np.squeeze(batch_lbls)

    # batch_negs: 2d array: (batch_size, num_neg) in this case, we use other pos_v as neg_v
    # batch_negs = np.tile(np.repeat(np.expand_dims(batch_lbls, 1).transpose(), batch_lbls.shape[0], axis=0), 2)
    batch_negs = np.repeat(np.expand_dims(batch_lbls, 1).transpose(), batch_lbls.shape[0], axis=0)

    # To long tensor
    batch_inps = torch.LongTensor(batch_inps)
    batch_tgts = torch.LongTensor(batch_tgts)
    batch_negs = torch.LongTensor(batch_negs)

    # Cuda
    batch_inps = Variable(batch_inps.cuda()) if elements['args'].cuda else Variable(batch_inps)
    batch_tgts = Variable(batch_tgts.cuda()) if elements['args'].cuda else Variable(batch_tgts)
    batch_negs = Variable(batch_negs.cuda()) if elements['args'].cuda else Variable(batch_negs)

    # history
    inps = NlpTools.populate_arrays(inps, batch_inps.numpy())
    tgts = NlpTools.populate_arrays(tgts, batch_tgts.numpy())
    negs = NlpTools.populate_arrays(negs, batch_negs.numpy())

    if verbose:
      print('batch_inps:', batch_inps.shape)
      print(batch_inps)
      print('batch_tgts:', batch_tgts.shape)
      print(batch_tgts)
      print('batch_negs:', batch_negs.shape)
      print(batch_negs)

    # Zero gradient
    elements['net'].zero_grad()

    # Forward and get loss
    loss = elements['net'](batch_inps, batch_tgts, batch_negs)

    # Backward
    loss.backward()

    # Step the optimizer
    elements['optimizer'].step()

    avg_loss += loss.item() # data[0]
    losses.append(avg_loss)

    if step % elements['args'].avg_step == 0:
      if step > 0:
        avg_loss /= elements['args'].avg_step
      print('\tAverage loss at iteration %6d:' % step, avg_loss)
      avg_loss = 0

    if step % elements['args'].ckpt_step == 0:
      # Get embeddings of valid words and perform L2-normalization
      valid_embeddings = elements['net'].forward_v(elements['batch_valid'])
      valid_embeddings = valid_embeddings.data.cpu().numpy() if elements['args'].cuda else valid_embeddings.data.numpy()
      valid_embeddings = normalize(valid_embeddings, norm='l2', axis=1)

      # Get embeddings of all words and perform L2-normalization
      embeddings = elements['net'].get_v_embeddings().cpu().numpy() if elements['args'].cuda else elements['net'].get_v_embeddings().numpy()
      normalized_embeddings = normalize(embeddings, norm='l2', axis=1)

      # Compute cosine similarity between valid words and all words in dictionary
      sim = np.matmul(valid_embeddings, np.transpose(normalized_embeddings))

      # Print top-k neighbors for each valid word
      for i in range(elements['args'].valid_size):
        valid_word = elements['reverse_dictionary'][elements['valid_examples'][i]]
        top_k = 8  # number of nearest neighbors
        nearest = (-sim[i, :]).argsort()[1:top_k + 1]

        log_str = '\tNearest to %-10s :' % valid_word
        for k in range(top_k):
          close_word = elements['reverse_dictionary'][nearest[k]]
          log_str = '%s %-16s' % (log_str, close_word)

  # graph
  fig, ax = plt.subplots(1, 4)
  fig.set_figwidth(15)
  ax[0].set_title('Inps')
  ax[0].hist(inps)
  del(inps) # mémoire
  ax[0].set_xlabel('xlabel')
  ax[0].set_ylabel('ylabel')

  ax[1].set_title('tgts')
  ax[1].hist(tgts)
  del(tgts) # mémoire

  ax[2].set_title('negs')
  ax[2].hist(negs)
  del(negs) # mémoire

  ax[3].set_title('Average loss')
  ax[3].plot(losses)
  del(losses) # mémoire
  ax[3].set_xlabel('Itération')
  ax[3].set_ylabel('losse')

def save_model(net, dictionary, reverse_dictionary, args, os):
  print("Étape 6 : Enregistrement du modèle formé")
  embeddings = net.get_v_embeddings().cpu().numpy() if args.cuda else net.get_v_embeddings().numpy()
  final_embeddings = normalize(embeddings, norm='l2', axis=1)

  model_dir = './models'
  NlpTools.maybe_create_path(model_dir)
  model_path = os.path.join(model_dir, 'word2vec.model')

  print('\tSaving trained weights to %s' % model_path)
  pickle.dump([final_embeddings, dictionary, reverse_dictionary], open(model_path, 'wb'))

def parse_arguments():
  parser = argparse.ArgumentParser()
  parser.add_argument('--cuda', type=NlpTools.str2bool, default='true')
  parser.add_argument('--gpuid', type=int, default=3)
  parser.add_argument('--verbose', type=int, default=0)
  parser.add_argument('--iterations', type=int, default=1000)
  parser.add_argument('--learning_rate', type=float, default=1.0)

  return parser.parse_args()

def main(p_args, file):
  words = download_data(file)
  data, count, dictionary, reverse_dictionary, args = build_dictionary(words, p_args.iterations, p_args.verbose, p_args.learning_rate)
  del(words) # mémoire
  index = 0
  batch, labels = make_training_batch(data, index, reverse_dictionary)
  net, optimizer, valid_examples, batch_valid = create_model(args, os, p_args.verbose)

  elements = {
    'args': args,
    'data': data,
    'index': index,
    'reverse_dictionary': reverse_dictionary,
    'net': net,
    'optimizer': optimizer,
    'valid_examples': valid_examples,
    'batch_valid': batch_valid
  }

  train_model(elements, p_args.verbose)
  save_model(net, dictionary, reverse_dictionary, args, os)

if __name__ == '__main__':
  hyper_parameters()
  dir = 'books'
  for file in os.listdir(dir):
    main(parse_arguments(), dir + '/' + file)
