# Installation
https://aws.amazon.com/fr/blogs/machine-learning/get-started-with-deep-learning-using-the-aws-deep-learning-ami/

```
yum group install "Development Tools"
```

## local

```
sudo apt install python3 python3-venv python3.6-tk
python3 -m venv v-env
source v-env/bin/activate
pip install -r requirements.txt
# pip install jupyter sklearn matplotlib scipy pylint plotly
# pip install https://download.pytorch.org/whl/cpu/torch-1.1.0-cp36-cp36m-linux_x86_64.whl
# pip install torchvision
# pip freeze > requirements.txt
```

# Connexion

```
# chmod 0400 ${PEM}
cd ~/Documents/AMI/
DNS=ec2-52-215-183-121.eu-west-1.compute.amazonaws.com
PEM=aws-ec2-ami.pem
ssh -L localhost:8888:localhost:8888 -i ${PEM} ec2-user@${DNS}
```

## Lancer Jupyter
```
jupyter notebook
```

# Installation pytorch

```
conda install pytorch-cpu torchvision-cpu -c pytorch
```

# Utilisation

```
python train_skip_gram.py --iterations 100 --learning_rate 0.2
python tsne.py --n_components 2 --plot_only 100 --perplexity 1.5 --learning_rate 0.3 --init 'pca'
```
