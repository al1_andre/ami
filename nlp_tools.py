#!/usr/bin/env python
# coding: utf-8

import os
import random
import zipfile
import collections
import numpy as np
import matplotlib.pyplot as plt

from tempfile import gettempdir
from six.moves import urllib

class FakeArguments:
  def __init__(self, vocabulary_size, num_steps=100, verbose=0, learning_rate=1):
    self.avg_step = int(num_steps / 10) # iteration à laquelle on imprime le loss moyen
    self.ckpt_step = int(num_steps / 2) # iteration à laquelle on lance un test
    self.num_steps = num_steps # Nombre total d'itérations
    self.cuda = NlpTools.str2bool('false')
    self.gpuid = 3
    self.learning_rate = learning_rate
    self.vocabulary_size = vocabulary_size

    if verbose:
      print('\tTaille du dictionnaire :', self.vocabulary_size)

    print("\n\tCalcule des paramètres de configuration")
    self.vocabulary_size = self.vocabulary_size

    size = int(0.128*100/self.vocabulary_size)
    self.embedding_size = size if size > 0 else 2

    size = int(0.002*100/self.vocabulary_size)
    self.num_skips = size if size > 0 else 2

    size = int(0.064*100/self.vocabulary_size)
    self.batch_size = size if size > 0 else int(self.num_skips*2)
    assert self.batch_size % self.num_skips == 0
    self.num_sampled = self.batch_size

    self.skip_window = int(self.num_skips/2)
    assert self.num_skips <= 2 * self.skip_window

    # Le nombre de mots à tester
    size = int(0.016*100/self.vocabulary_size)
    self.valid_size = size if size > 0 else 1

    # la position maximale dans laquelle prendre un mot à tester
    size = int(0.1*100/self.vocabulary_size)
    self.valid_window = size if size > 0 else 2

    if verbose:
      print('\tvocabulary_size :', self.vocabulary_size)
      print('\tembedding_size :', self.embedding_size)
      print('\tbatch_size :', self.batch_size)
      print('\tnum_skips :', self.num_skips)
      print('\tskip_window :', self.skip_window)
      print('\tnum_sampled :', self.num_sampled)
      print('\tvalid_size :', self.valid_size)
      print('\tvalid_window :', self.valid_window)

class NlpTools():
  def __init__():
    super(NlpTools, self).__init__()

  def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
      return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
      return False

  def maybe_create_path(path):
    if not os.path.exists(path):
      os.mkdir(path)
      print("Created a path: %s" % path)

  def maybe_download(url, file_name, expected_bytes):
    """Download a file if not present, and make sure it's the right size."""
    local_file_name = os.path.join(gettempdir(), file_name)

    if not os.path.exists(local_file_name):
      local_file_name, _ = urllib.request.urlretrieve(url + file_name, local_file_name)

    stat_info = os.stat(local_file_name)

    if stat_info.st_size == expected_bytes:
      print('Found and verified', file_name)
    else:
      print(stat_info.st_size)
      raise Exception('Failed to verify' + local_file_name + '. Can you get to it with a browser?')

    return local_file_name

  def extract_file(file_name, position):
    """Extract the file enclosed at position in a zip file as a list of words."""
    with zipfile.ZipFile(file_name) as f:
      vocabs = (f.read(f.namelist()[position])).split()
    return vocabs

  def build_skipgram_dataset(words, iterations=1000, verbose=0, learning_rate=1):
    """
    Building the dataset for Skip Gram model
    :param words: 1d Array of words
    :param num_words: The number of words in the dictionary
    :return:
    """
    count = [['UNK', -1]]
    count.extend(collections.Counter(words).most_common(len(words) - 1))

    dictionary = dict()
    index, indexes, freqs = 0, [], []
    for word, freq in count:
      dictionary[word] = len(dictionary)
      indexes.append(index)
      freqs.append(freq)
      index += 1

    args = FakeArguments(index, iterations, verbose, learning_rate) # parse_arguments()

    data = list()
    unk_count = 0
    for word in words:
      index = dictionary.get(word, 0)
      if index == 0: # dictionary['UNK']
        unk_count += 1
      data.append(index)

    count[0][1] = unk_count
    reversed_dictionary = dict(zip(dictionary.values(), dictionary.keys()))

    # graph
    fig, ax = plt.subplots()
    ax.plot(indexes, freqs)
    ax.set_title('Fréquence des mots du jeu de données')
    ax.set_xlabel('Échantillon')
    ax.set_ylabel('Fréquence')

    return data, count, dictionary, reversed_dictionary, args

  def generate_batch(data, index, batch_sz, n_skips, skip_sz, verbose=0):
    if verbose:
      print('batch_sz', batch_sz, 'n_skips', n_skips)

    assert batch_sz % n_skips == 0
    assert n_skips <= 2 * skip_sz

    inputs = np.ndarray(shape=batch_sz, dtype=np.int64)
    labels = np.ndarray(shape=batch_sz, dtype=np.int64)

    span = 2 * skip_sz + 1
    buff = collections.deque(maxlen=span)

    if index + span > len(data):
      index = 0

    buff.extend(data[index:index + span])
    index += span

    for i in range(batch_sz // n_skips):
      context_words = [w for w in range(span) if w != skip_sz]
      words_to_use = random.sample(context_words, n_skips)

      for j, context_word in enumerate(words_to_use):
        inputs[i * n_skips + j] = buff[skip_sz]
        labels[i * n_skips + j] = buff[context_word]

      if index == len(data):
        buff.extend(data[0:span])
        index = span
      else:
        buff.append(data[index])
        index += 1

    # Backtrack a little bit to avoid skipping words in the end of a batch
    index = (index + len(data) - span) % len(data)
    return inputs, labels, index

  def populate_arrays(origin, np_to_add):
    for index, item in enumerate(np.nditer(np_to_add)):
      origin.append(item)
    return origin