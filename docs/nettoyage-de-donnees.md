Sur un corpus de 2 million de mots

# Les jetons les plus utilisés
Ces jetons (tokens) sont des mots clés : à, de, (, ;, etc.) ils sont très importants pour le contexte.

# Les jetons les moins utilisés
Ces jetons n'apparaissent pas suffisement de fois pour que l'on puisse créer des vecteurs avec un contexte interpretable. 33k apparaissent moins de 5 fois, il faut les neutraliser afin d'éviter de perdre du temps de calcul.

il seront connu dans les vecteurs comme "UNKNOWN" et leur fréquence moyenne (ici 1.81) leur est affecté. (cf graph https://lirnli.wordpress.com/2017/11/03/notes-on-word-vectors/)

#
Un mot a un context. Ce context a une valeur après entrainement, on affiche pour chaque mot la valeur de ce context