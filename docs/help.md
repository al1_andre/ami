# CRF
Les Conditional Random Field sont une classe de modèles statistiques utilisés en reconnaissance des formes et plus généralement en apprentissage statistique. Les CRFs permettent de prendre en compte l'interaction de variables "voisines", ils sont souvent utilisés pour des données séquentielles (langage naturel, séquences biologiques, vision par ordinateur).

Les CRFs sont un exemple de réseau probabiliste non orienté.

# RNN
Les réseaux de neurones récurrents conviennent en particulier pour l'analyse de séries temporelles. Ils sont utilisés en reconnaissance automatique de la parole ou de l'écriture manuscrite - plus en général en reconnaissance de formes.

# Affichage

## t-SNE
t-Distributed Stochastic Neighbor Embedding (t-SNE) est une technique de réduction de dimension (méthode non-linéaire permettant de représenter un ensemble de points d'un espace à grande dimension dans un espace de deux ou trois dimensions) pour la visualisation de données (elles peuvent ensuite être visualisées avec un nuage de points)

## PCA
L'analyse en composantes principales (Principal Component Analysis) consiste à transformer des variables liées entre elles (dites "corrélées") en nouvelles variables décorrélées les unes des autres. Elle permet de réduire le nombre de variables et de rendre l'information moins redondante.